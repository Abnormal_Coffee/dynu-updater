# Dynu Updater

[![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-rust.svg)](https://forthebadge.com)

## Installation
### Manual
- Download the project
- Insure that you have cargo installed
- Run `cargo build --release`
- Once compiled, copy the binary from `target/release/dynu-updater`
- Paste file wherever you want to keep it
- Execute with `./dynu-update` - might want to give execute permissions
### Premade Build <- Easier Option
- Download relevant build from `builds/`
- Paste the binary wherever you want to keep it
- Execute with `./dynu-update` - might want to give execute permissions

## Usage
- Create `.env` file in same directory as binary
- Add the following to the .env file:
    - `DYNU_ALIAS="{alias name}"` replace {alias name} with the alias
    - `DYNU_HOSTNAME="{hostname}"` replace {hostname} with the hostname
    - `UPDATE_INTERVAL="{update interval}"` replace {update interval} with an integer that represents the minutes between updates
    - `DYNU_PASSWORD_SHA256="{password}"` replace {password} with a sha512 hash of your IP update password

## Roadmap
- CLI args?

## Platforms

- [x] Linux
- [x] Windows
- [x] MacOS; not able to test or cross-compile at the moment
- [ ] BSD; ¯\\_(°_°))_/¯
- [ ] Android; ¯\\_(°_°))_/¯
use dotenv::dotenv;
use public_ip;
use std::env;
use tokio::{time::sleep, time::Duration};
use ureq;

#[tokio::main]
async fn main() {
    dotenv().ok();
    let alias = env::var("DYNU_ALIAS").expect("DYNU_ALIAS not found in env file");
    let hostname = env::var("DYNU_HOSTNAME").expect("DYNU_HOSTNAME not found in env file");
    let update_interval: u64 = env::var("UPDATE_INTERVAL")
        .expect("UPDATE_INTERVAL not found in env file")
        .parse()
        .expect("Failed to parse update interval");
    let password =
        env::var("DYNU_PASSWORD_SHA256").expect("DYNU_PASSWORD_SHA256 not found in env file");

    let mut ipv4 = public_ip::addr_v4()
        .await
        .expect("Failed to retreive ipv4")
        .to_string();
    ip_update_request(&ipv4, &hostname, &alias, &password);
    loop {
        sleep(Duration::from_secs(update_interval * 60)).await;
        if let Some(ip) = public_ip::addr_v4().await {
            ipv4 = ip.to_string();
            ip_update_request(&ipv4, &hostname, &alias, &password);
        }
    }
}

fn ip_update_request(
    ipv4: &str,
    /*ipv6: &str, */ hostname: &str,
    alias: &str,
    password: &str,
) {
    let path = format!("http://api.dynu.com/nic/update?hostname={hostname}&alias={alias}&myip={ipv4}&password=sha256({password})");
    let res = ureq::get(&path).call();
    println!("{res:?}");
}
